JC = javac
JVM = java

SRCDIR = src/
LIB = lib/lwjgl/*:lib/joml-1.9.12.jar:lib/PNGDecoder.jar:

MAIN = engineTester/MainGameLoop

SRCMODULES = $(shell find src/ -name "*.java")
CLASSES = $(SRCMODULES:.java=.class)

%.class: %.java
	@echo -e "\e[32mCompiling \`\e[39;1m$<\e[32;21;24m'\e[39m"
	@$(JC) -cp $(SRCDIR):$(LIB) $<

build: $(CLASSES)
	@echo -n ""

run: $(CLASSES)
	@echo -e "\e[36mRunning \`\e[39;1m$(MAIN)\e[36;21;24m'\e[39m"
	@$(JVM) -cp $(SRCDIR):$(LIB) $(MAIN)

clean:
	rm -f $(CLASSES)
