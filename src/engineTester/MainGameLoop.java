package engineTester;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.io.File;

import org.joml.*;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

import renderEngine.DisplayManager;
import renderEngine.Loader;
import renderEngine.MasterRenderer;

import shaders.StaticShader;
import models.RawModel;
import models.TexturedModel;
import textures.ModelTexture;
import entities.Entity;
import entities.Camera;
import entities.Light;
import entities.Player;
import terrains.Terrain;
import textures.TerrainTexture;
import textures.TerrainTexturePack;
import input.Mouse;
import guis.GuiTexture;
import guis.GuiRenderer;
import toolbox.MousePicker;
import water.WaterRenderer;
import water.WaterShader;
import water.WaterTile;
import water.WaterFrameBuffers;
import normalMappingObjConverter.NormalMappedObjLoader;
import fontRendering.TextMaster;
import fontMeshCreator.GUIText;
import fontMeshCreator.FontType;
import particles.ParticleMaster;
import particles.Particle;
import particles.ParticleTexture;
import particles.ParticleSystemSimple;
import particles.ParticleSystemComplex;
import postProcessing.Fbo;
import postProcessing.PostProcessing;

public class MainGameLoop {

	private static List<Entity> entities = new ArrayList<Entity>();
	private static List<Entity> normalMapEntities = new ArrayList<Entity>();
	private static List<Light> lights = new ArrayList<Light>();
	private static List<Terrain> terrains = new ArrayList<Terrain>();
	private static List<GuiTexture> guis = new ArrayList<GuiTexture>();
	private static List<WaterTile> waters = new ArrayList<WaterTile>();


	public static void placeRandomly(
		TexturedModel model,
		Terrain terrain,
		float scale,
		int count,
		int indexesCount,
		float yPos
	) {
		Random random = new Random();
		for(int i=0;i<count;i++){
			float x = random.nextFloat()*800 - 400;
			float z = random.nextFloat() * -800 + 400;
			float y = terrain.getHeightOfTerrain(x, z) + yPos;
			int index = random.nextInt(indexesCount);
			entities.add(new Entity(
				model,
				index,
				new Vector3f(x, y, z),
				0, 0, 0, scale
			));
		}
	}

	public static void placeRandomly(TexturedModel model, Terrain terrain, float scale, int count) {
		placeRandomly(model, terrain, scale, count, 1, 0);
	}

	public static void main(String[] args) {
		DisplayManager.createDisplay();
		Loader loader = new Loader();
		TextMaster.init(loader);


		/* ------------------- Player ------------------- */

		ModelTexture bunnyTexture = new ModelTexture(loader.loadTexture("white"));
		TexturedModel bunnyModel = new TexturedModel(loader.loadOBJFileToVAO("stanfordBunny"), bunnyTexture);

		Player player = new Player(bunnyModel, new Vector3f(120, 0, -100), 0, -180, 0, .2f);
		entities.add(player);

		Camera camera = new Camera(player);



		MasterRenderer renderer = new MasterRenderer(loader, camera);
		ParticleMaster.init(loader, renderer.getProjectionMatrix());

		/* ------------------- Terrain ------------------- */

		TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grassy"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("dirt"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("pinkFlowers"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));
		TerrainTexturePack texturePack = new TerrainTexturePack(
			backgroundTexture,
			rTexture, gTexture, bTexture
		);
		TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendMap"));

		Terrain terrain = new Terrain(-0.5f, -0.5f, loader, texturePack, blendMap);
		//Terrain terrain2 = new Terrain(-1, -1, loader, texturePack, blendMap, "heightmap");
		terrains.add(terrain);


		/* ------------------- Models ------------------- */

		ModelTexture treeTexture = new ModelTexture(loader.loadTexture("pine"));
		TexturedModel treeModel = new TexturedModel(loader.loadOBJFileToVAO("pine"), treeTexture);

		ModelTexture tree2Texture = new ModelTexture(loader.loadTexture("lowPolyTree"));
		TexturedModel tree2Model = new TexturedModel(loader.loadOBJFileToVAO("lowPolyTree"), tree2Texture);

		ModelTexture fernTexture = new ModelTexture(loader.loadTexture("fern"));
		fernTexture.setNumberOfRows(2);
		fernTexture.setHasTransparency(true);
		TexturedModel fernModel = new TexturedModel(loader.loadOBJFileToVAO("fern"), fernTexture);

		ModelTexture grassTexture = new ModelTexture(loader.loadTexture("grassTexture"));
		TexturedModel grassModel = new TexturedModel(loader.loadOBJFileToVAO("grassModel"), grassTexture);
		grassTexture.setHasTransparency(true);
		grassTexture.setUseFakeLighting(true);

		ModelTexture flowerTexture = new ModelTexture(loader.loadTexture("flower"));
		TexturedModel flowerModel = new TexturedModel(loader.loadOBJFileToVAO("grassModel"), flowerTexture);
		flowerTexture.setHasTransparency(true);
		flowerTexture.setUseFakeLighting(true);

		ModelTexture cherryTexture = new ModelTexture(loader.loadTexture("cherry"));
		TexturedModel cherryModel = new TexturedModel(loader.loadOBJFileToVAO("cherry"), cherryTexture);
		cherryModel.getTexture().setHasTransparency(true);
		cherryModel.getTexture().setShineDamper(10);
		cherryModel.getTexture().setReflectivity(0.5f);
		cherryModel.getTexture().setSpecularMap(loader.loadTexture("cherryS"));

		ModelTexture lanternTexture = new ModelTexture(loader.loadTexture("lantern"));
		TexturedModel lanternModel = new TexturedModel(loader.loadOBJFileToVAO("lantern"), lanternTexture);
		lanternModel.getTexture().setHasTransparency(true);
		lanternModel.getTexture().setShineDamper(10);
		lanternModel.getTexture().setReflectivity(0.5f);
		lanternModel.getTexture().setSpecularMap(loader.loadTexture("lanternS"));


		/* ------------------- NM Entities ------------------- */

		TexturedModel barrelModel = new TexturedModel(
			//NormalMappedObjLoader.loadOBJ("barrel", loader),
			NormalMappedObjLoader.loadOBJ("barrel", loader),
			new ModelTexture(loader.loadTexture("barrel"))
		);
		barrelModel.getTexture().setNormalMap(loader.loadTexture("barrelNormal"));
		barrelModel.getTexture().setShineDamper(10);
		barrelModel.getTexture().setReflectivity(0.5f);

		Entity barrel = new Entity(barrelModel, new Vector3f(150, -4.5f, -105), 0, 0, 0, 1f);
		//normalMapEntities.add(barrel);


		/* ------------------- Placing Entities ------------------- */

		placeRandomly(treeModel, terrain, 1, 500);
		//placeRandomly(cherryModel, terrain, 1, 500);
		//placeRandomly(lanternModel, terrain, 1, 500);
		placeRandomly(tree2Model, terrain, 3, 500, 1, -5);
		placeRandomly(fernModel, terrain, 1f, 500, 4, 0);
		//placeRandomly(grassModel, terrain, 1f, 500);
		//placeRandomly(flowerModel, terrain, 1.2f, 500);


		/* ------------------- Light ------------------- */

		ModelTexture lampTexture = new ModelTexture(loader.loadTexture("lamp"));
		lampTexture.setUseFakeLighting(true);
		TexturedModel lampModel = new TexturedModel(loader.loadOBJFileToVAO("lamp"), lampTexture);

		Light sun = new Light(new Vector3f(1000000, 1500000, -1000000), new Vector3f(1.0f, 1.0f, 1.0f));
		lights.add(sun);
		lights.add(new Light(
			new Vector3f(185, 10, -293), new Vector3f(2, 0, 0), new Vector3f(1, 0.01f, 0.002f)));
		lights.add(new Light(
			new Vector3f(370, 17, -300), new Vector3f(0, 2, 2), new Vector3f(1, 0.01f, 0.002f)));
		lights.add(new Light(
			new Vector3f(293, 7, -305), new Vector3f(2, 2, 0), new Vector3f(1, 0.01f, 0.002f)));

		entities.add(new Entity(lampModel, new Vector3f(185, -4.7f, -293), 0, 0, 0, 1));
		entities.add(new Entity(lampModel, new Vector3f(370, 4.2f, -300), 0, 0, 0, 1));
		entities.add(new Entity(lampModel, new Vector3f(293, -6.8f, -305), 0, 0, 0, 1));


		/* ------------------- Water ------------------- */

		WaterFrameBuffers buffers = new WaterFrameBuffers();
		WaterShader waterShader = new WaterShader();
		WaterRenderer waterRenderer = new WaterRenderer(
			loader, waterShader, renderer.getProjectionMatrix(), buffers);
		WaterTile water = new WaterTile(0, 0, -5);
		waters.add(water);


		/* ------------------- GUI ------------------- */

		GuiRenderer guiRenderer = new GuiRenderer(loader);
		GuiTexture shadowMap = new GuiTexture(
			renderer.getShadowMapTexture(),
			new Vector2f(0.5f, 0.5f), new Vector2f(0.5f, 0.5f)
		);
		//guis.add(shadowMap);


		/* ------------------- Particle System ------------------- */

		ParticleTexture particleTexture = new ParticleTexture(
			loader.loadTexture("particleAtlas"), 4
		);
		//ParticleSystemSimple system = new ParticleSystemSimple(50, 25, 0.3f, 4);
		ParticleSystemComplex system
			= new ParticleSystemComplex(particleTexture, 50, 25, 0.3f, 4, 1);
		system.randomizeRotation();
		system.setDirection(new Vector3f(0, 1, 0), 0.1f);
		system.setLifeError(0.1f);
		system.setSpeedError(0.4f);
		system.setScaleError(0.8f);


		/* ------------------- Post Processing ------------------- */

		Fbo multisampleFbo = new Fbo(
			DisplayManager.getWidth(), DisplayManager.getHeight()
		);
		Fbo outputFbo = new Fbo(
			DisplayManager.getWidth(), DisplayManager.getHeight(),
			Fbo.DEPTH_TEXTURE
		);
		Fbo outputFbo2 = new Fbo(
			DisplayManager.getWidth(), DisplayManager.getHeight(),
			Fbo.DEPTH_TEXTURE
		);
		PostProcessing.init(loader);


		/* ------------------- Main Game Loop ------------------- */


		while (!DisplayManager.isCloseRequested()) {
			player.move(terrain);
			camera.move();
			//barrel.increaseRotation(0, .6f, 0);

			//system.generateParticles(new Vector3f(120, -14, -110));
			ParticleMaster.update(camera);

			renderer.renderShadowMap(entities, sun);
			GL11.glEnable(GL30.GL_CLIP_DISTANCE0);

			// Reflection
			buffers.bindReflectionFrameBuffer();
			float distance = 2 * (camera.getPosition().y - water.getHeight());
			camera.getPosition().y -= distance;
			camera.invertPitch();
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, 1, 0, -water.getHeight()+.3f));
			camera.getPosition().y += distance;
			camera.invertPitch();

			// Refraction
			buffers.bindRefractionFrameBuffer();
			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, -1, 0, water.getHeight()+.3f));

			// Screen
			GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
			buffers.unbindCurrentFrameBuffer();

			multisampleFbo.bindFrameBuffer();

			renderer.renderScene(entities, normalMapEntities, terrains, lights, camera, new Vector4f(0, 0, 0, 0));
			waterRenderer.render(waters, camera, sun);

			ParticleMaster.renderParticles(camera);
			multisampleFbo.unbindFrameBuffer();
			multisampleFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT0, outputFbo);
			multisampleFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT1, outputFbo2);
			//multisampleFbo.resolveToScreen();
			PostProcessing.doPostProcessing(outputFbo.getColourTexture(), outputFbo2.getColourTexture());

			guiRenderer.render(guis);
			TextMaster.render();

			DisplayManager.updateDisplay();
			Mouse.postUpdate();
		}

		/* ------------------- Cleaning Up ------------------- */

		PostProcessing.cleanUp();
		multisampleFbo.cleanUp();
		outputFbo.cleanUp();
		outputFbo2.cleanUp();
		ParticleMaster.cleanUp();
		TextMaster.cleanUp();
		buffers.cleanUp();
		guiRenderer.cleanUp();
		renderer.cleanUp();
		loader.cleanUp();
		DisplayManager.closeDisplay();
	}
}
