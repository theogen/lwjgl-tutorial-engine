package input;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;

import renderEngine.DisplayManager;

public final class Mouse {
	private static GLFWScrollCallback scrollCallback;
	private static GLFWCursorPosCallback cursorPosCallback;

	private static long window;

	private static boolean scrollChanged = false;
	private static boolean mouseChanged = false;

	private static float mouseDWheel;
	private static float mousePosX;
	private static float mousePosY;
	private static float mousePrevPosX;
	private static float mousePrevPosY;
	private static float mouseDX;
	private static float mouseDY;

	static {
		window = DisplayManager.getWindow();

		scrollCallback = GLFWScrollCallback.create((win, dx, dy) ->
		{
			mouseDWheel = (float)dy;
			scrollChanged = true;
		});
		GLFW.glfwSetScrollCallback(window, scrollCallback);

		cursorPosCallback = GLFWCursorPosCallback.create((win, posx, posy) ->
		{
			mousePosX = (float)posx;
			mousePosY = (float)posy;
			mouseDX = mousePosX - mousePrevPosX;
			mouseDY = mousePosY - mousePrevPosY;
			mousePrevPosX = mousePosX;
			mousePrevPosY = mousePosY;
			mouseChanged = true;
		});
		GLFW.glfwSetCursorPosCallback(window, cursorPosCallback);
	}

	public static void postUpdate() {
		if (scrollChanged) {
			mouseDWheel = 0;
			scrollChanged = false;
		}

		if (mouseChanged) {
			mouseDX = 0;
			mouseDY = 0;
			mouseChanged = false;
		}
	}

	public static boolean getMouseButton(int index) {
		int button = GLFW.GLFW_MOUSE_BUTTON_LEFT;
		if (index == 1)
			button = GLFW.GLFW_MOUSE_BUTTON_RIGHT;
		if (index == 2)
			button = GLFW.GLFW_MOUSE_BUTTON_MIDDLE;
		return 1 == GLFW.glfwGetMouseButton(window, button);
	}

	public static float getDWheel()
	{
		return mouseDWheel;
	}

	public static float getPosX() {
		return mousePosX;
	}

	public static float getPosY() {
		return mousePosY;
	}

	public static float getDX() {
		return mouseDX;
	}

	public static float getDY() {
		return mouseDY;
	}
}
