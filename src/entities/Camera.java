package entities;

import org.lwjgl.glfw.GLFW;

import org.joml.Vector3f;

import renderEngine.DisplayManager;
import input.Mouse;

public class Camera {
	private float distanceFromPlayer = 10;
	private float angleAroundPlayer = 0;

	private Vector3f position = new Vector3f(0, 5, 0);
	private float pitch = 20;
	private float yaw = 0;
	private float roll = 0;

	private Player player;

	public Camera(Player player) {
		this.player = player;
	}

	public void move() {
		calculateZoom();
		calculatePitch();
		calculateAngleAroundPlayer();

		float horizontalDistance = calculateHorizontalDistance();
		float verticalDistance = calculateVerticalDistance();

		calculateCameraPosition(horizontalDistance, verticalDistance);
		this.yaw = 180 - (player.getRotY() + angleAroundPlayer);
	}

	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public void invertPitch() {
		pitch = -pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}

	private void calculateCameraPosition(float horizDistance, float verticDistance) {
		float theta = player.getRotY() + angleAroundPlayer;
		float offsetX = (float)(horizDistance * Math.sin(Math.toRadians(theta)));
		float offsetZ = (float)(horizDistance * Math.cos(Math.toRadians(theta)));
		position.x = player.getPosition().x - offsetX;
		position.z = player.getPosition().z - offsetZ;
		position.y = player.getPosition().y + verticDistance;
	}

	private float calculateHorizontalDistance() {
		return (float)(distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
	}

	private float calculateVerticalDistance() {
		return (float)(distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
	}

	private void calculateZoom() {
		float zoomLevel = Mouse.getDWheel() * .8f;
		distanceFromPlayer -= zoomLevel;
	}

	private void calculatePitch() {
		if (Mouse.getMouseButton(1)) {
			float pitchChange = Mouse.getDY() * .1f;
			pitch += pitchChange;
		}
	}

	private void calculateAngleAroundPlayer() {
		if (Mouse.getMouseButton(0)) {
			float angleChange = Mouse.getDX() * .3f;
			angleAroundPlayer -= angleChange;
		}
	}
}
