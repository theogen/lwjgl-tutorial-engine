# LWJGL Tutorial Engine

[channel]: https://www.youtube.com/channel/UCUkRj4qoT1bsWpE_C8lZYoQ
[playlist]: https://www.youtube.com/playlist?list=PLRIWtICgwaX0u7Rf9zkZhLoLuZVfUksDP
[lwjgl]: https://www.lwjgl.org/
[jdk]: https://en.wikipedia.org/wiki/Java_Development_Kit
[make]: https://www.gnu.org/software/make/

This is my very first OpenGL game engine that I created following
[ThinMatrix][channel]'s [tutorial series][playlist] back in January 2019.

It doesn't follow the series exactly and it isn't complete either (I have
stopped somewhere at #47, the bloom tutorial). Also, it uses [LWJGL][lwjgl]
version 3 instead of 2.

All files under `src/` and `res/` are public domain (see
[`LICENSE`](./LICENSE)). All libraries under `lib/` are under their respective
licenses that you can find alongside them.

This repository is more for historical and educational purposes rather than
practical. All credit goes to [ThinMatrix][channel]. Thank you for teaching me
OpenGL in such an awesome and entertaining way!

## Building

You need [Java Development Kit (JDK)][jdk] and [GNU Make][make] to build the
engine. Simply type `make` to build and `make run` to run the example game.

## Example game

![screenshot.jpg](./screenshot.jpg)

You are playing as a stanford bunny in a flooded forest with giant trees. It's
not really a game, more of a tech demo, but it's kind of fun to run around for
a while in this surreal world. If you need a goal... try to find the lamppost
from the screenshot. ;-)

### Controls

- **WASD** to move.
- **Space** to jump.
- **Scroll wheel** to zoom in/out.
- **LMB** to move camera around.
- **RMB** to move camera up/down.
